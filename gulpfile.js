var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var path = require('path');
var args = require('yargs').argv;

var locationConfig = require('./tools/config/location.config');
var mockServerConfig = require('./tools/config/mock-server.config.js');
var webpackDevServerConfig = require('./tools/webpack/dev-server.config.js');

gulp.task('clean', clean);

gulp.task('build', build());

gulp.task('demo', gulp.series(
    build(),
    startServer,
    openBrowser
));

gulp.task('dev', gulp.series(
    enableDev,
    build(),
    startServer,
    openBrowser,
    test
));

gulp.task('test', test);

function clean() {
    var del = require('del');

    return del(locationConfig.dist.location);
}

function build() {
    return gulp.series(
        clean,
        gulp.parallel(
            buildIndex,
            buildCssVendors,
            buildJsVendors,
            buildWebpack
        )
    );
}

function buildIndex() {
    return gulp.src(locationConfig.index)
        .pipe(gulp.dest(locationConfig.dist.location));
}

function buildCssVendors() {
    return gulp.src([
        require.resolve('bootstrap/dist/css/bootstrap.min.css')
    ])
        .pipe(removeSourceMappingURLs())
        .pipe($.concat('vendors.css'))
        .pipe(gulp.dest(locationConfig.dist.css));
}

function buildJsVendors() {
    return gulp.src([
        require.resolve('jquery/dist/jquery.min.js'),
        require.resolve('bootstrap/dist/js/bootstrap.min.js')
    ])
        .pipe(removeSourceMappingURLs())
        .pipe($.concat('vendors.js'))
        .pipe(gulp.dest(locationConfig.dist.js));
}

function removeSourceMappingURLs() {
    return $.replace(/sourceMappingURL=/g, '');
}

function buildWebpack(done) {
    var webpack = require('webpack');
    var webpackConfig;

    if(!args.dev) {
        webpackConfig = require('./tools/webpack/config.prod.js');

        webpack(webpackConfig, function(error) {
            exitOnError(error);

            done();
        });

        return;
    }

    webpackConfig = require('./tools/webpack/config.dev.js');

    var compiler = webpack(webpackConfig);
    completeOnSuccessfulCompile(compiler, done);

    var WebpackDevServer = require('webpack-dev-server');
    var webpackDevServer = new WebpackDevServer(compiler, webpackDevServerConfig.options);
    webpackDevServer.listen(webpackDevServerConfig.port, webpackDevServerConfig.host, function(error) {
        exitOnError(error);

        console.info('Webpack dev server listening on port ' + webpackDevServerConfig.port);
    });

    function exitOnError(error) {
        if(!error) {
            return;
        }

        console.error('ERROR: Webpack Build Failed');
        console.error('- ' + error.name);
        console.error('- ' + error.message);

        process.exit(1);
    }

    function completeOnSuccessfulCompile(compiler, done) {
        compiler.plugin('done', function() {
            done();
        });
    }
}

function enableDev(done) {
    args.dev = true;
    done();
}

function startServer(done) {
    var nodemon = require('nodemon');
    nodemon({
        script: mockServerConfig.location,
        watch: mockServerConfig.watch
    })
        .once('start', done);
}

function openBrowser(done) {
    var opn = require('opn');
    var serverAddress = args.dev ? webpackDevServerConfig.address : mockServerConfig.address;

    opn(serverAddress);
    done();
}

function test(done) {
    var KarmaServer = require('karma').Server;

    new KarmaServer({
        configFile: locationConfig.karmaConfig,
        singleRun: !args.dev
    }, done).start();
}