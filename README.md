# Weather forecast app

##Pre-requisites

### Node Version

This repo requires v6.4.0

### To run:

```
npm install --production
npm start
```

### To test:

```
npm install
npm test
```

### About

This project uses OpenWeatherMap 5 day forecast API. It gives the user the opportunity to select the location of their choice and show's what the next 5 day forecast has to bring.

NB! Note: At this time, the app is still in development and so it's not connecting directly to the API but rather calling the data from a stubbed JSON file for development speed.

### TODO (what further I'd like to acheive)
* Further unit testing
* Load locations dynamically into dropdown
* Implement immutable library
* Build a strategy to switch between dev (stub files) and prod (API call)
* Cache data locally so the app can be used offline
* Use a ticker on the range input field
* Use GMAPS API to display a map of the location the user selects
* Animate the 'timeline'
* Animate/add graphics to reflect the weather
* Add styling, cross-browser/device testing
* Add functionality to choose other forecast periods
* Capture users information to store preferences / favourites