var cloneWith = require('../utils/clone-with');
var serverAddress = require('../utils/server-address');
var locationConfig = require('../config/location.config');
var mockServerConfig = require('../config/mock-server.config');

var devServerConfig = {
    protocol: 'http',
    host: 'localhost',
    port: 5000
};

module.exports = cloneWith(devServerConfig, {
    address: serverAddress(devServerConfig),
    options: {
        hot: true,
        publicPath: locationConfig.publicPath,
        proxy: {
            '*': mockServerConfig.address
        },
        stats: 'errors-only'
    }
});
