var path = require('path');
var express = require('express');

var locationConfig = require('../config/location.config');
var serverConfig = require('./config');

var app = express();
app.use(express.static(locationConfig.dist.location));
app.use(express.static(locationConfig.stubs));

app.get('*', function(req, res) {
  res.sendFile(path.join(locationConfig.dist.location, 'index.html'));
});

var server = app.listen(serverConfig.port, function() {
    console.info('Mock server listening on port ' + serverConfig.port);
});

module.exports = server;