var path = require('path');

var cloneWith = require('../utils/clone-with');
var serverAddress = require('../utils/server-address');
var config = require('../mock-server/config');

var serverSrc = path.join(__dirname, '../', 'mock-server');

module.exports = cloneWith(config, {
    address: serverAddress(config),
    location: serverSrc,
    watch: path.join(serverSrc, '**/*')
});