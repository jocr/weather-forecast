var path = require('path');

var root = path.resolve(__dirname, '../../');
var src = path.join(root, 'src');
var dist = path.join(root, 'dist');

module.exports = {
    src: src,
    dist: {
        location: dist,
        js: path.join(dist, 'js'),
        css: path.join(dist, 'css')
    },
    stubs: path.join(root, 'tools/mock-server/data'),
    index: path.join(src, 'index.html'),
    sass: path.join(src, '**', '*.scss'),
    publicPath: '/',
    karmaConfig: path.join(__dirname, './karma.config.js')
};
