import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from '../reducers';

export default (initialState) => {
    let enhancer = applyMiddleware(thunk);
    if(window.devToolsExtension) {
        enhancer = compose(
            enhancer,
            window.devToolsExtension && window.devToolsExtension()
        );
    }

    return createStore(
        rootReducer,
        initialState,
        enhancer
    );
};
