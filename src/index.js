import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import configureStore from './store';
import Root from './root';

const store = configureStore();
const root = document.getElementById('root');

render(
    <AppContainer>
        <Root store={store} />
    </AppContainer>,
    root
);

if(module.hot) {
    module.hot.accept('./root', () => {
        const NextRoot = require('./root').default;
        render(
            <AppContainer>
                <NextRoot store={store} />
            </AppContainer>,
            root
        );
    });
}
