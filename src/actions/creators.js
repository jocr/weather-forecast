import * as types from './types';
import fetch from 'isomorphic-fetch';

// abstract out to api-service
// store apiKey in config.js
// http://api.openweathermap.org/data/2.5/forecast?id=${locationId}&appid=${apiKey}&units=metric
const getLocation = (locationId) => {
	return (dispatch) => {
		fetch(`/${locationId}.json`)
      .then(response => response.json())
      .then(json =>
        dispatch(locationFetchSuccess(json))
     	)
  };
};

export { locationFetchSuccess, changeLocation };

function locationFetchSuccess(locationData) {
  return { type: types.CHANGE_LOCATION_SUCCESS, locationData }
}

function changeLocation(newLocation) {
	return getLocation(newLocation);
}