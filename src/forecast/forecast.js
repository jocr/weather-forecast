import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';

import { changeLocation } from '../actions';
import styles from './forecast.scss';

import { ForecastViewer } from './forecastViewer';

export const Forecast = ({ forecast, changeLocation }) => {
    const forecastViewer = _.isEmpty(forecast) ?
        null : 
        <ForecastViewer locationData={forecast} />;

    return (
        <div>
            <h1>Five Day Forecast</h1>
            <div>
                <label>Select your location:</label>
                <select onChange={(event) => changeLocation(event.target.value)}>
                    <option value=''>Please select an option...</option>
                    <option value='524901'>Moscow</option>
                    <option value='2648110'>Greater London</option>
                </select>
            </div>
            {forecastViewer}
        </div>
    );
};

Forecast.propTypes = {
    forecast: PropTypes.object.isRequired,
    changeLocation: PropTypes.func.isRequired
};

const mapStateToProps = ({ forecast }) => ({ forecast });

const mapDispatchToProps = dispatch => ({
    changeLocation(newLocation) {
        dispatch(changeLocation(newLocation));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Forecast);
