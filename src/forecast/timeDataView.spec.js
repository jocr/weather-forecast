import React from 'react';
import { TimeDataView } from './timeDataView';

import { shallow } from 'enzyme';

describe('<TimeDataView /> component', () => {
    let props = {};
    let wrapper;

    beforeEach(() => {
        props.timeData = {};
    });

    afterEach(() => {
        let modules = Object.keys(require.cache);

        modules.forEach(moduleName => {
            delete require.cache[moduleName];
        });
    });

    it('should render nine <p> elements', () => {
        wrapper = shallow(<TimeDataView {...props} />);
        expect(wrapper.find('p').length).toEqual(9);
    });

    describe('passing through data', () => {
        beforeEach(() => {
            props.timeData = {
                dt: '',
                main: {},
                weather: [{}],
                clouds: {},
                wind: {},
                rain: {}
            };
        });

        it('displays the weather data that\'s passed through props (wind)', () => {
            props.timeData.wind = {
                speed: 123,
                deg: 340
            };
            let windDescription = `Wind: ${props.timeData.wind.speed} meter/sec, ${props.timeData.wind.deg} degrees`;
            wrapper = shallow(<TimeDataView {...props} />);
            expect(wrapper.find('p').at(7).text()).toEqual(windDescription);
        });
    });
});
