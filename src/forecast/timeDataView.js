import React, { PropTypes } from 'react';
import moment from 'moment';
import styles from './forecast.scss';

export const TimeDataView = ({ timeData }) => {
  const { dt, main: main = {}, weather = [{}], clouds = {}, wind = {}, rain = {} } = timeData;
  const { temp: temperature = 0, temp_min: temp_min = 0, temp_max: temp_max = 0, 
    humidity: humidity = 0, pressure: pressure = 0 } = main;
  const { main: mainWeather = '', description: description = '' } = weather[0];
  const { all: cloudPerc = 0 } = clouds;
  const { speed: windSpeed = 0, deg: windDeg = 0 } = wind;
  const last3HoursRain = rain && rain['3h'] ? rain['3h'] : 0;
  const dateTime = moment.unix(dt).format('ddd DD MMM YYYY - HH:mm');
  
  return (
    <div>
      <p>Date: {dateTime}</p>
      <p>Overview: {mainWeather} - {description} - {temperature}&deg;C</p>
      <p>Min: {temp_min}&deg;C</p>
      <p>Max: {temp_max}&deg;C</p>
      <p>Humidity: {humidity}%</p>
      <p>Pressure: {pressure} hPa</p>
      <p>Clouds: {cloudPerc}%</p>
      <p>Wind: {windSpeed} meter/sec, {windDeg} degrees</p>
      <p>Rain (last 3 hours): {last3HoursRain}mm</p>
    </div>
  );
};

TimeDataView.propTypes = {
    timeData: React.PropTypes.shape({
      main: PropTypes.object.isRequired,
      weather: React.PropTypes.arrayOf(React.PropTypes.object),
      clouds: PropTypes.object.isRequired,
      wind: PropTypes.object.isRequired,
      rain: PropTypes.object.isRequired
    })
};