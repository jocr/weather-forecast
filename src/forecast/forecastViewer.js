import React, { PropTypes } from 'react';
import moment from 'moment';

import { TimeDataView } from './timeDataView';

import styles from './forecast.scss';

export class ForecastViewer extends React.Component {

	constructor(props) {
    super(props);
    this.state = {
    	timeDataIndex: 0
    };
  }

  updateTimeIndex(value) {
  	this.setState({
  		timeDataIndex: value
  	});
  }

  render() {
  	const { locationData } = this.props;
  	const locationTitle = locationData && locationData.city ? `${locationData.city.name} (${locationData.city.country})` : null;
		const allTimeData = locationData && locationData.list ? locationData.list : [];
		const maxRange = locationData.cnt - 1;
		const timeData = allTimeData.length && allTimeData[this.state.timeDataIndex] ? allTimeData[this.state.timeDataIndex] : {};
		return (
	    <div className={styles.forecastViewer}>
	      <h2 className={styles.location}>{locationTitle}</h2> 
	      <label htmlFor="time_slider">Drag slider to view weather forecast for the next 5 days</label>
	      <input 
	      	className={styles.forecastSlider}
	      	id="time_slider"
	      	type="range" min="0" max={maxRange} 
	      	value={this.state.timeDataIndex} 
	      	onChange={(event) => this.updateTimeIndex(event.target.value)} />
	      <TimeDataView timeData={timeData} />
	    </div>
	  );
  }
};

ForecastViewer.propTypes = {
    locationData: PropTypes.object.isRequired
};