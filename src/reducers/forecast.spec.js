import * as actionCreators from '../actions/creators';

describe('forecast reducer', () => {
    let forecast;
    let initialState;
    beforeEach(() => {
        forecast = require('./forecast').default;
        initialState = forecast(undefined, {});
    });

    afterEach(() => {
        let modules = Object.keys(require.cache);

        modules.forEach(moduleName => {
            delete require.cache[moduleName];
        });
    });

    it('initially returns empty object', () => {
        expect(initialState).toEqual({});
    });

    describe('action changeLocation', () => {
        let currentState;

        beforeEach(() => {
            currentState = initialState;
        });

        it('changes from empty object to forecast data on success', () => {
            expect(currentState).toEqual({});

            let locationData = { temp: '123' }

            const newState = forecast(currentState, actionCreators.locationFetchSuccess(locationData));

            expect(newState).toEqual(locationData);
        });
    });
});
