import * as types from '../actions/types';

import cloneWith from '../../tools/utils/clone-with';

let initialState = {};

export default (state = initialState, action) => {
    switch(action.type) {
        case types.CHANGE_LOCATION_SUCCESS:
            return cloneWith(state, action.locationData);
        default:
            return state;
    }
};
