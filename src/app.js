import React from 'react';
import Forecast from './forecast/forecast';

const App = () => (
	<div className="container">
    <Forecast />
	</div>
);

export default App;
